// Intro with Banner
const intro = document.querySelector(".intro");
intro.textContent = "</JETT>";

//GSAP for Sections
const introText = document.querySelector(".intro-text");
const sectionsSlide = document.querySelectorAll(".sections-slide");
const sectionsRotate = document.querySelectorAll(".sections-rotate");
const controller = new ScrollMagic.Controller();

const slideSections = () => {
  //Loop over each section
  sectionsSlide.forEach((section) => {
    const transform = section.querySelector(".transform");
    //GSAP
    const slideSceneTl = gsap.timeline({ defaults: { ease: "power1.out" } });
    slideSceneTl.fromTo(transform, { y: "20%" }, { y: "0%", duration: 1 });
    slideSceneTl.fromTo(
      transform,
      { opacity: 0 },
      { opacity: 1, duration: 1 },
      "-=1"
    );
    //Create Scene
    const slideScene = new ScrollMagic.Scene({
      triggerElement: section,
      triggerHook: 0.8,
      reverse: false,
    })
      .setTween(slideSceneTl)
      .addTo(controller);
  });
};
const rotateSections = () => {
  //Loop over each section
  sectionsRotate.forEach((section) => {
    //New Anim
    const transformRotate = section.querySelector(".transform-rotate");
    const journeySceneTl = gsap.timeline({ ease: "power1.out" });
    journeySceneTl.fromTo(
      transformRotate,
      { y: "-90%" },
      { y: "0%", duration: 0.5 }
    );
    journeySceneTl.fromTo(
      transformRotate,
      { rotationX: 90 },
      { rotationX: 0, duration: 0.5 },
      "-=.5"
    );
    journeySceneTl.fromTo(
      transformRotate,
      { opacity: 0.2 },
      { opacity: 1, duration: 0.5 },
      "-=.5"
    );
    // //Create New Scene
    const journeyScene = new ScrollMagic.Scene({
      triggerElement: section,
      triggerHook: 0.8,
      reverse: false,
    })
      .setTween(journeySceneTl)
      .addTo(controller);
  });
};
// const lightSections = () => {
//   //Loop over each section
//   sectionsRotate.forEach((section) => {
//     //New Anim
//     const journeySpan = section.querySelectorAll(".journey-btn span");
//     const journeyBtn = section.querySelector(".journey-btn");

//     const journeyOpacityTl = gsap.timeline({
//       defaults: { ease: "power1.out" },
//       onComplete: () => {
//         // Check if the button has not been clicked
//         if (!journeyBtn.dataset.clicked) {
//           // Set the data-clicked attribute to "true"
//           journeyBtn.dataset.clicked = "true";
//           // Programmatically click the button
//           journeyBtn.click();
//         }
//       },
//     });
//     journeyOpacityTl.to(journeySpan, { opacity: 1, duration: 0.5 });
//     // //Create New Scene
//     const journeyOpacityScene = new ScrollMagic.Scene({
//       triggerElement: section,
//       triggerHook: 0.7,
//     })
//       .setTween(journeyOpacityTl)
//       .addTo(controller);
//   });
// };
const lightSections = () => {
  // Loop over each section
  sectionsRotate.forEach((section) => {
    // Query the necessary elements
    const journeyElements = section.querySelectorAll(".journey-btn span");
    const journeyBtn = section.querySelector(".journey-btn");

    // Timeline for "lights on" effect
    const journeyOpacityOnTl = gsap.timeline({
      defaults: { ease: "power1.out" },
      onComplete: () => {
        // Trigger a click event programmatically only if not already clicked
        if (!journeyBtn.classList.contains("clicked")) {
          journeyBtn.classList.add("clicked");
          journeyBtn.click();
        }
      },
    });

    journeyOpacityOnTl.to(
      journeyElements,
      // { opacity: 0 },
      { opacity: 1, duration: 0.8 }
    );

    // Timeline for "lights off" effect
    // const journeyOpacityOffTl = gsap.timeline({
    //   defaults: { ease: "power1.out" },
    // });

    // journeyOpacityOffTl.to(journeyElements, { opacity: 0, duration: 1 });

    // Create ScrollMagic Scenes
    const journeyOpacityOnScene = new ScrollMagic.Scene({
      triggerElement: section,
      triggerHook: 0.9,
      // reverse: true,
    })
      .setTween(journeyOpacityOnTl)
      .addTo(controller);

    // const journeyOpacityOffScene = new ScrollMagic.Scene({
    //   triggerElement: section,
    //   triggerHook: 0.1,
    //   // reverse: true, // Ensures reverse is active for "lights off"
    // })
    //   .setTween(journeyOpacityOffTl)
    //   .addTo(controller);
  });
};

// GSAP for intro
const introButton = document.querySelector(".intro-btn");
const tl = gsap.timeline({ defaults: { ease: "power1.out" } });
const tl2 = gsap.timeline({ defaults: { ease: "power2.inOut" } });

const seePortfolio = () => {
  document.querySelector(".page-wrapper").style.cssText =
    "opacity: 1; visibility: visible";
  document.querySelector(".container").style.overflow = "visible";
  tl.set(".name-img", { transformOrigin: "left top" });
  tl.fromTo(introButton, { y: "0%" }, { y: "80%", duration: 0.2 });
  tl2.to(".slider", { y: "-100%", duration: 1.5 });
  tl.fromTo(".banner", { opacity: 1 }, { opacity: 0, duration: 0.5 });
  tl.fromTo(
    ".navbar-link",
    { opacity: 0 },
    { opacity: 1, duration: 1 },
    "+=.5"
  );
  tl.fromTo(".name-img", { opacity: 0 }, { opacity: 1, duration: 0.5 });
  tl.fromTo(".name-img", { rotation: 10 }, { rotation: 0, duration: 1 });
  tl.fromTo(".name", { y: "20%" }, { y: "0%", duration: 1 });
  tl.fromTo(".name", { opacity: 0 }, { opacity: 1, duration: 1 }, "-=1");
  introButton.removeEventListener("click", seePortfolio);
  slideSections();
  rotateSections();
  lightSections();

  document.body.style.backgroundColor = "#060606";
};
introButton.addEventListener("click", seePortfolio);

// Logo
const logo = document.querySelector("#logo");
logo.textContent = "</Jett>";

// Mouse Circle
let mouseText = document.querySelector(".mouse-dot span");
const pointers = document.querySelectorAll(".pointer-enter");

// window.addEventListener("scroll", arrowToggle);

// document.body.addEventListener("mousemove", (e) => {
//   let x = e.clientX;
//   let y = e.clientY;

//   mouseCircleFn(x, y);
//   activeCursor(e);
// });

// document.body.addEventListener("mouseleave", () => {
//   mouseCircle.style.opacity = "0";
//   mouseDot.style.opacity = "0";
// });

// End of Mouse Circle

// Progress Bar
const sections = document.querySelectorAll("section");
const progressBar = document.querySelector(".progress-bar");
const halfCircles = document.querySelectorAll(".half-circle");
const halfCircleTop = document.querySelector(".half-circle-top");
const progressBarCircle = document.querySelector(".progress-bar-circle");

let scrolledPortion = 0;
let scrollBool = false;
let imageWrapper = false;

const progressBarFn = (bigImgWrapper) => {
  imageWrapper = bigImgWrapper;
  let pageHeight = 0;
  const pageViewportHeight = window.innerHeight;

  if (!imageWrapper) {
    pageHeight = document.documentElement.scrollHeight;
    scrolledPortion = window.pageYOffset;
  } else {
    pageHeight = imageWrapper.firstElementChild.scrollHeight;
    scrolledPortion = imageWrapper.scrollTop;
  }

  const scrolledPortionDegree =
    (scrolledPortion / (pageHeight - pageViewportHeight)) * 360;

  halfCircles.forEach((el) => {
    el.style.transform = `rotate(${scrolledPortionDegree}deg)
`;

    if (scrolledPortionDegree >= 180) {
      halfCircles[0].style.transform = "rotate(180deg)";
      halfCircleTop.style.opacity = "0";
    } else {
      halfCircleTop.style.opacity = "1";
    }
  });

  scrollBool = scrolledPortion + pageViewportHeight === pageHeight;

  // Arrow Rotation
  if (scrollBool) {
    progressBarCircle.style.transform = "rotate(180deg)";
  } else {
    progressBarCircle.style.transform = "rotate(0)";
  }
  // End of Arrow Rotation
};

// Progress Bar Click
progressBar.addEventListener("click", (e) => {
  e.preventDefault();

  if (!imageWrapper) {
    const sectionPositions = Array.from(sections).map(
      (section) => scrolledPortion + section.getBoundingClientRect().top
    );

    const position = sectionPositions.find((sectionPosition) => {
      return sectionPosition > scrolledPortion;
    });

    scrollBool ? window.scrollTo(0, 0) : window.scrollTo(0, position);
  } else {
    scrollBool
      ? imageWrapper.scrollTo(0, 0)
      : imageWrapper.scrollTo(0, imageWrapper.scrollHeight);
  }
});
// End of Progress Bar Click

progressBarFn();

// End of Progress Bar

// Navigation
const menuIcon = document.querySelector(".menu-icon");
const navbar = document.querySelector(".navbar");

const scrollFn = () => {
  menuIcon.classList.add("show-menu-icon");
  navbar.classList.add("hide-navbar");

  if (window.scrollY === 0) {
    menuIcon.classList.remove("show-menu-icon");
    navbar.classList.remove("hide-navbar");
  }

  progressBarFn();
};

document.addEventListener("scroll", scrollFn);

menuIcon.addEventListener("click", () => {
  menuIcon.classList.remove("show-menu-icon");
  navbar.classList.remove("hide-navbar");
});
// End of Navigation

// About Me Text
const introTextContent =
  "Hi, I'm Jett. I'm a self-taught front-end developer that loves to create and design. I look forward to meeting you. :)";

Array.from(introTextContent).forEach((char) => {
  const span = document.createElement("span");
  span.textContent = char;
  introText.appendChild(span);

  span.addEventListener("mouseenter", (e) => {
    e.target.style.animation = "aboutMeTextAnim 10s infinite";
  });
});
// End of About Me Text

// Projects

const decreaseAmntAllWorkProjectsShown = () => {
  const allWork = document.getElementById("All Work");
  allWork.innerHTML = `
    <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-8.png"
                          alt="JobBoardAPI Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/jobBoardApi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/jobBoardApi"
                            class="pointer-enter" 
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Remote Now</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          for real time job postings.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-6.png"
                          alt="Hello Photo Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/photowebsite/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/photowebsite"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Hello Photo</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          professional photos with Pexel's API and display them on
                          a page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-1.png"
                          alt="MockYoutube Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/mockyoutubeapi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/mockyoutubeapi"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >MockYoutube</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          current videos with Youtube's API and display them on a
                          page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                  <div class="project-card-center">
                    <div class="project pointer-enter project-card-img">
                      <img
                        src="images/projects/project-7.png"
                        alt="Beyond Beats"
                      />
                      <div>
                        <a
                          href="https://jettdembin.github.io/beatMaker/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/beatMaker"
                          class="pointer-enter"
                          target="_blank"
                          >Github</a
                        >
                      </div>
                    </div>
                    <div class="project-card-text-center">
                      <h1 >Beyond Beats</h1>
                      <p>
                        Vanilla Javascript Beat Maker application that displays
                        OOP concepts.
                      </p>
                      <h2>
                        Technologies Used: <br />HTML5 | CSS3 | JavaScript
                      </h2>
                    </div>
                  </div>
              
                </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-9.png"
                          alt="TodoList Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/todoList/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/todoList"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >TodoList</h1>
                        <p>
                          Vanilla Javascript todo list. The todo list is saved to
                          the browser's local storage, and can be loaded upon page
                          refresh. The todo list can be sorted by completed,
                          uncompleted, or all. Todos also animate upon completion
                          and deletion of todos.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-2.png"
                          alt="Quiz Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/quizApp/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/quizApp"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Ecommerce Quiz</h1>
                        <p>
                          Vanilla Javascript quiz application that displays
                          questions dynamically upon entering them in the
                          questions.js file.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>                
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-10.png"
                          alt="Macro Meals"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/calculator/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/calculator"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Macro Meals</h1>
                        <p>
                          React Application which calculates user calories based
                          on desired fitness goal and allows you to track your
                          meals and see how many calories you have consumed.
                        </p>
                        <h2>Technologies Used: <br />HTML5 | CSS3 | React</h2>
                      </div>
                    </div>            
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-11.png"
                          alt="Ecommerce Landing Page"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/ecommerce/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/ecommerce"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Your Ecommerce</h1>
                        <p>
                          React ecommerce landing page built with the mobile first
                          approach.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | React
                          Responsive Carousel | Framer Motion
                        </h2>
                      </div>
                    </div> 
                  </div>
  `;
};

const increaseAmntAllWorkProjectsShown = () => {
  const allWork = document.getElementById("All Work");
  allWork.innerHTML = `
    <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-10.png"
                          alt="Macro Meals"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/calculator/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/calculator"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1>Macro Meals</h1>
                        <p>
                          React Application which calculates user calories based
                          on desired fitness goal and allows you to track your
                          meals and see how many calories you have consumed.
                        </p>
                        <h2>Technologies Used: <br />HTML5 | CSS3 | React</h2>
                      </div>
                    </div>
                    <div class="project-card-left">                    
                      <div class="project-card-text">
                        <h1 >Your Ecommerce</h1>
                        <p>
                          React ecommerce landing page built with the mobile first
                          approach.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | React | React
                          Responsive Carousel | Framer Motion
                        </h2>
                      </div>                      
                      <div class="project pointer-enter project-card-img">
                        <img
                        src="images/projects/project-11.png"
                        alt="Ecommerce Landing Page"
                        />
                        <div>
                        <a
                          href="https://jettdembin.github.io/ecommerce/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/ecommerce"
                          target="_blank"
                          class="pointer-enter"
                          >Github</a
                        >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-6.png"
                          alt="Hello Photo Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/photowebsite/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/photowebsite"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Hello Photo</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          professional photos with Pexel's API and display them to user.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">                    
                      <div class="project-card-text">
                        <h1 >Remote Now</h1>
                        <p>
                          Vanilla JavaScript job board application built to allow users to see the display
                          for real time job postings.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>                      
                      <div class="project pointer-enter project-card-img">
                        <img
                        src="images/projects/project-8.png"
                        alt="JobBoardAPI Project"
                      />
                      <div>
                        <a
                          href="https://jettdembin.github.io/jobBoardApi/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/jobBoardApi"
                          class="pointer-enter"
                          target="_blank"
                          >Github</a
                        >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-7.png"
                          alt="Beyond Beats"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/beatMaker/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/beatMaker"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Beyond Beats</h1>
                        <p>
                          Vanilla JavaScript Beat Maker application programmed with OOP concepts. Allows users to create beats with a kicker, high-hat, and snare. User may increase, decrease, and change the tempo of the beat.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-1.png"
                          alt="MockYoutube Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/mockyoutubeapi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/mockyoutubeapi"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >MockYoutube</h1>
                        <p>
                          Vanilla Javascript application built to allow users to search
                          current videos with Youtube's API and display them on a
                          page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-2.png"
                          alt="Quiz Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/quizApp/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/quizApp"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Ecommerce Quiz</h1>
                        <p>
                          Vanilla Javascript quiz application that displays
                          questions dynamically upon entering them in the
                          questions.js file. Simulates pagination with back and next button. Allows for restart of quiz and randomization of questions with each quiz.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-9.png"
                          alt="TodoList Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/todoList/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/todoList"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >TodoList</h1>
                        <p>
                          Vanilla Javascript todo list. The todo list is saved to
                          the browser's local storage, and can be loaded upon page
                          refresh. The todo list can be sorted by completed,
                          uncompleted, or all. Todos also animate upon completion
                          and deletion of todos.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>`;
};

// $(window).on("load resize orientationchange ready", () => {
//   if ($(window).width() < 1200) {
//     decreaseAmntAllWorkProjectsShown();
//     document
//       .querySelector(".project-selector-cntr-mirror-main")
//       .classList.add("project-selector-cntr-mirror-secondary");
//     document
//       .querySelector(".project-content")
//       .classList.add("project-content-secondary");
//   } else {
//     increaseAmntAllWorkProjectsShown();
//     document
//       .querySelector(".project-selector-cntr-mirror-main")
//       .classList.remove("project-selector-cntr-mirror-secondary");
//     document
//       .querySelector(".project-content")
//       .classList.remove("project-content-secondary");
//   }
// });

const container = document.querySelector(".container");
const pageWrapper = document.querySelector(".page-wrapper");
let projects = document.querySelectorAll(".project");
const projectHideBtn = document.querySelector(".project-hide-btn");
const projImg = document.querySelectorAll(".project img");

const projectAnimations = () => {
  //if window is greater than phone size
  if ($(window).width() > 650) {
    projects.forEach((project, i) => {
      //show div that contains demo and github buttons
      project.addEventListener("mouseenter", () => {
        project.firstElementChild.style.top = `-${
          project.firstElementChild.offsetHeight - project.offsetHeight + 150
        }px`;
        project.lastElementChild.style.opacity = "1";
        project.lastElementChild.style.animation = "none";
      });

      project.addEventListener("mouseleave", () => {
        project.firstElementChild.style.top = "2rem";
        if (window.innerWidth > 900) {
          project.lastElementChild.style.opacity = "0";
          project.lastElementChild.style.animation = "none";
        }
      });

      //prevent event bubbling on big image if user clicked links to see website or code
      project.lastElementChild.firstElementChild.addEventListener(
        "click",
        (e) => {
          e.stopPropagation();
        }
      );
      project.lastElementChild.lastElementChild.addEventListener(
        "click",
        (e) => {
          e.stopPropagation();
        }
      );

      // Big Project Image
      // Event listener for the main project element
      project.addEventListener("click", (e) => {
        // Prevent multiple quick successive clicks
        if (!e.detail || e.detail === 1) {
          // Hide UI elements
          menuIcon.classList.remove("show-menu-icon");
          navbar.classList.add("hide-navbar");

          // Container for the demo and GitHub links
          const codeLinks = document.createElement("div");
          codeLinks.className = "code-links";
          container.appendChild(codeLinks);

          // Create and append the demo link
          const codelink = document.createElement("a");
          codelink.textContent = "Demo";
          codelink.className = "code-link pointer-enter";
          codelink.style.padding = "2rem 2.5rem 1rem 2.5rem";
          const linkPath =
            project.lastElementChild.firstElementChild.getAttribute("href");
          codelink.setAttribute("href", linkPath);
          codelink.target = "_blank";
          codeLinks.appendChild(codelink);

          // Create and append the GitHub link
          const codelink2 = document.createElement("a");
          codelink2.textContent = "Github";
          codelink2.className = "code-link pointer-enter";
          const linkPath2 =
            project.lastElementChild.lastElementChild.getAttribute("href");
          codelink2.setAttribute("href", linkPath2);
          codelink2.target = "_blank";
          codeLinks.appendChild(codelink2);

          // Create the close button
          const closeBtn = document.createElement("button");
          closeBtn.innerHTML = `<i class="fas fa-times"></i>`;
          closeBtn.classList.add("project-hide-btn", "change");
          codeLinks.appendChild(closeBtn);

          // Big image wrapper and image creation
          const bigImgWrapper = document.createElement("div");
          bigImgWrapper.className = "project-img-wrapper";
          container.appendChild(bigImgWrapper);

          const bigImg = document.createElement("img");
          bigImg.className = "project-img";
          const imgPath = project.firstElementChild
            .getAttribute("src")
            .split(".")[0];
          bigImg.setAttribute("src", `${imgPath}-big.png`);
          bigImgWrapper.appendChild(bigImg);
          document.body.style.overflowY = "hidden";

          // Handle scrolling on the big image
          bigImgWrapper.addEventListener("scroll", () => {
            // Code for handling scroll-linked animations
          });

          // Remove the global scroll listener
          document.removeEventListener("scroll", scrollFn);

          // Initialize the progress bar function
          progressBarFn(bigImgWrapper);

          // Scroll event for the big image wrapper
          bigImgWrapper.onscroll = () => {
            progressBarFn(bigImgWrapper);
          };

          // Function to close the project view
          function closeProjectView() {
            closeBtn.classList.remove("change");
            codeLinks.remove();
            bigImgWrapper.remove();
            document.body.style.overflowY = "scroll";
            document.addEventListener("scroll", scrollFn);
            progressBarFn();
          }

          // Event listener for the close button
          closeBtn.addEventListener("click", closeProjectView);

          // Stop propagation for demo and GitHub link clicks
          codelink.addEventListener("click", () => {
            closeProjectView();
            console.log("clicked");
          });
          codelink2.addEventListener("click", () => {
            closeProjectView();
            console.log("CLICKED");
          });
        }
      });

      // End of Big Project Image
    });
  } else {
    projImg.forEach((img) => {
      img.style.opacity = 1;
    });
  }
};

//listen to load, resize, orientation change to adjust for demo and github animations
$(window).on("load resize orientationchange ready", projectAnimations);
// Projects Button
const section3 = document.querySelector(".section-3");

// End of Projects Button

//Project Tab Selector
const setNewActive = (el, e) => {
  e.stopPropagation();
  const contentBodies = document.getElementsByClassName("content-body");
  for (let contentBody of contentBodies) {
    contentBody.classList.remove("show-flex");
    contentBody.classList.remove("testimonial-slider1");
  }
  //change height of project container
  if (e.currentTarget.textContent === "All Work") {
    if ($(window).width() > 1200) {
      document
        .querySelector(".project-selector-cntr-mirror-main")
        .classList.remove("project-selector-cntr-mirror-secondary");
      document
        .querySelector(".project-content")
        .classList.remove("project-content-secondary");
      document.querySelector(".section-4").classList.remove("secondary");
    }
  } else {
    document
      .querySelector(".project-selector-cntr-mirror-main")
      .classList.add("project-selector-cntr-mirror-secondary");
    document
      .querySelector(".project-content")
      .classList.add("project-content-secondary");
    document.querySelector(".section-4").classList.add("secondary");
  }
  //show corresponding projects
  if (document.getElementById(el.textContent.trim()) === "All Work") {
    document
      .getElementById(el.textContent.trim())
      .classList.add("show-flex", "testimonial-slider1");
    document
      .querySelector(".project-card")

      .classList.remove("project-card-secondary");
  } else {
    document
      .getElementById(el.textContent.trim())
      .classList.add("show-flex", "testimonial-slider1");
    document.getElementById(el.textContent.trim()).style.cssText =
      "opacity: 1; visibility: visible;";
  }
  el.removeEventListener("click", setNewActive);
  el.addEventListener("click", setNewActive);
};

//Function for active tab border underline
const tabs = document.getElementsByClassName("tab");
const tabHeaders = document.querySelectorAll(".tab h6");
const activeTab = (e) => {
  e.stopPropagation();
  e.target.classList.add("selected");
  e.target.style.color = "rgb(298,298,298)";
  e.currentTarget.removeEventListener("click", activeTab);
  e.currentTarget.addEventListener("click", activeTab);
};

const removeTabBorder = () => {
  for (let tab of tabs) {
    tab.classList.remove("selected");
    tab.style.border = "none";
    tab.style.color = "rgba(0, 0, 0, 0.701)";
  }
};

for (let tab of tabs) {
  tab.addEventListener("click", (e) => {
    setNewActive(e.currentTarget, e);
    removeTabBorder();
    activeTab(e);
    slickSliderWithResize();
  });
}
// End Project Tab Selector
// End of Projects ********************************

// Section 4
const showJourneyAnimation = (e, cntr) => {
  e.stopPropagation();
  e.preventDefault();
  const journeyText = cntr.nextElementSibling;
  journeyText.classList.toggle("change");
  const rightPosition = journeyText.classList.contains("change")
    ? `calc(100% - ${getComputedStyle(cntr.firstElementChild).width})`
    : 0;
  cntr.firstElementChild.style.right = rightPosition;
  e.currentTarget.removeEventListener("click", showJourneyAnimation);
  e.currentTarget.addEventListener("click", showJourneyAnimation);
};
document.querySelectorAll(".journey-btn").forEach((journey) => {
  journey.addEventListener("click", (e) => {
    showJourneyAnimation(e, journey);
  });
});
// End of Section 4

// Section 5
// Form
const formHeading = document.querySelector(".form-heading");
const formInputs = document.querySelectorAll(".contact-form-input");

formInputs.forEach((input) => {
  input.addEventListener("focus", () => {
    formHeading.style.opacity = "0";
    setTimeout(() => {
      formHeading.textContent = `Your ${input.placeholder}`;
      formHeading.style.opacity = "1";
      formHeading.style.textTransform = "uppercase";
    }, 300);
  });

  input.addEventListener("blur", () => {
    formHeading.style.opacity = "0";
    setTimeout(() => {
      formHeading.textContent = "...Don't click away :C";
      formHeading.style.textTransform = "unset";
      formHeading.style.opacity = "1";
    }, 300);
  });
});
// End of Form

// Form Validation
const form = document.querySelector(".contact-form");
const username = document.getElementById("name");
const email = document.getElementById("email");
const subject = document.getElementById("subject");
const message = document.getElementById("message");
const messages = document.querySelectorAll(".message");

const error = (input, message) => {
  input.nextElementSibling.classList.add("error");
  input.nextElementSibling.textContent = message;
};

const success = (input) => {
  input.nextElementSibling.classList.remove("error");
};

const checkRequiredFields = (inputArr) => {
  inputArr.forEach((input) => {
    if (input.value.trim() === "") {
      error(input, `${input.id} is required`);
    }
  });
};

const checkLength = (input, min) => {
  if (input.value.trim().length < min) {
    error(input, `${input.id} must be at least ${min} characters`);
  } else {
    success(input);
  }
};

const checkEmail = (input) => {
  const regEx =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  if (regEx.test(input.value.trim())) {
    success(input);
  } else {
    error(input, "Email is not valid");
  }
};

form.addEventListener("submit", (e) => {
  debugger;
  checkLength(username, 2);
  checkLength(subject, 2);
  checkLength(message, 10);
  checkEmail(email);
  checkRequiredFields([username, email, subject, message]);

  const notValid = Array.from(messages).find((message) => {
    return message.classList.contains("error");
  });

  notValid && e.preventDefault();
});
form.addEventListener("reset", (e) => {
  e.preventDefault();
  e.stopPropagation();
  const resumeBtn = document.getElementById("resume-btn");
  resumeBtn.dispatchEvent(new MouseEvent("click"));
  return;
});
// End of Form Validation
// End of Section 5

// Slick slideshow
const initSlickSlider = () => {
  $(".testimonial-slider1").slick({
    dots: true,
    infinite: false,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    prevArrow:
      "<button type='button' class='slick-prev custom-slick-arrow'><i class='fas fa-arrow-left' aria-hidden='true'></i></button>",
    nextArrow:
      "<button type='button' class='slick-next custom-slick-arrow'><i class='fas fa-arrow-right' aria-hidden='true'></i></button>",
  });
};

const slickSliderWithResize = () => {
  // Check if on react project section
  if ($(".testimonial-slider1").hasClass("react")) {
    $(".testimonial-slider1").slick("unslick");
  }

  const windowWidth = $(window).width();
  const isPortrait = window.matchMedia("(orientation: portrait)").matches;

  if (
    windowWidth > 1649 ||
    windowWidth < 1650 ||
    windowWidth < 1200 ||
    isPortrait
  ) {
    if ($(".testimonial-slider1").hasClass("slick-initialized")) {
      $(".testimonial-slider1").slick("unslick");
    }
    initSlickSlider();
  }
};

$(window).on("resize", slickSliderWithResize);
$(document).ready(slickSliderWithResize);
//End Slick Slideshow

//function to alert user the password for shopify websites
const shopifys = document.querySelectorAll(".shopify");
shopifys.forEach((shopify) => {
  shopify.addEventListener("click", () => {
    alert(
      `Please enter "welcome" as the password, without the quotations, to enter the site`
    );
  });
});
const shopifysNo = document.querySelectorAll(".shopify-no");
shopifysNo.forEach((shopify) => {
  shopify.addEventListener("click", () => {
    alert(
      `Unfortunately, this website requires payment with the theme I used to create it, thus you will not be able to view it here. Please inquire about it if you would like me to show you through screenshare or video. Thank you 🙂`
    );
  });
});

// End Project Tab

//Outro
const outroText = document.querySelector(".outro-logo h1");
outroText.textContent = "</JETT>";
const logoCntr = document.querySelector(".outro-logo-cntr");
const outroLogo = document.querySelector(".outro-logo");

//Function for logoBounce windowheight listener
function logoBounce() {
  const logoPos = logoCntr.getBoundingClientRect().top;
  const windowHeight = window.innerHeight / 2.5;

  if (logoPos < windowHeight) {
    outroLogo.style.animation = "logoBounce 3s ease 0.5s 1";
  }
}

window.addEventListener("scroll", logoBounce);
// Socials
const githubSocial = document.querySelector("#github");
const linkedinSocial = document.querySelector("#linkedin");
const socialsCntr = document.querySelector(".socials-cntr");
// offsetHeight: 147
// offsetLeft: 40
