export const decreaseAmntAllWorkProjectsShown = () => {
	const allWork = document.getElementById("All Work");
	allWork.innerHTML = `
    <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-8.png"
                          alt="JobBoardAPI Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/jobBoardApi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/jobBoardApi"
                            class="pointer-enter" 
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1>Remote Now</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          for real time job postings.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-6.png"
                          alt="Hello Photo Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/photowebsite/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/photowebsite"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Hello Photo</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          professional photos with Pexel's API and display them on
                          a page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-1.png"
                          alt="MockYoutube Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/mockyoutubeapi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/mockyoutubeapi"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >MockYoutube</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          current videos with Youtube's API and display them on a
                          page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                  <div class="project-card-center">
                    <div class="project pointer-enter project-card-img">
                      <img
                        src="images/projects/project-7.png"
                        alt="Beyond Beats"
                      />
                      <div>
                        <a
                          href="https://jettdembin.github.io/beatMaker/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/beatMaker"
                          class="pointer-enter"
                          target="_blank"
                          >Github</a
                        >
                      </div>
                    </div>
                    <div class="project-card-text-center">
                      <h1 >Beyond Beats</h1>
                      <p>
                        Vanilla Javascript Beat Maker application that displays
                        OOP concepts.
                      </p>
                      <h2>
                        Technologies Used: <br />HTML5 | CSS3 | JavaScript
                      </h2>
                    </div>
                  </div>
              
                </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-9.png"
                          alt="TodoList Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/todoList/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/todoList"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >TodoList</h1>
                        <p>
                          Vanilla Javascript todo list. The todo list is saved to
                          the browser's local storage, and can be loaded upon page
                          refresh. The todo list can be sorted by completed,
                          uncompleted, or all. Todos also animate upon completion
                          and deletion of todos.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-2.png"
                          alt="Quiz Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/quizApp/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/quizApp"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Ecommerce Quiz</h1>
                        <p>
                          Vanilla Javascript quiz application that displays
                          questions dynamically upon entering them in the
                          questions.js file.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>                
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-10.png"
                          alt="Macro Meals"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/calculator/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/calculator"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Macro Meals</h1>
                        <p>
                          React Application which calculates user calories based
                          on desired fitness goal and allows you to track your
                          meals and see how many calories you have consumed.
                        </p>
                        <h2>Technologies Used: <br />HTML5 | CSS3 | React</h2>
                      </div>
                    </div>            
                  </div>
                  <div class="project-card">
                    <div class="project-card-center">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-11.png"
                          alt="Ecommerce Landing Page"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/ecommerce/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/ecommerce"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text-center">
                        <h1 >Your Ecommerce</h1>
                        <p>
                          React ecommerce landing page built with the mobile first
                          approach.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | React
                          Responsive Carousel | Framer Motion
                        </h2>
                      </div>
                    </div> 
                  </div>
  `;
};

export const increaseAmntAllWorkProjectsShown = () => {
	const allWork = document.getElementById("All Work");
	allWork.innerHTML = `
    <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-10.png"
                          alt="Macro Meals"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/calculator/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/calculator"
                            target="_blank"
                            class="pointer-enter"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1>Macro Meals</h1>
                        <p>
                          React Application which calculates user calories based
                          on desired fitness goal and allows you to track your
                          meals and see how many calories you have consumed.
                        </p>
                        <h2>Technologies Used: <br />HTML5 | CSS3 | React</h2>
                      </div>
                    </div>
                    <div class="project-card-left">                    
                      <div class="project-card-text">
                        <h1 >Your Ecommerce</h1>
                        <p>
                          React ecommerce landing page built with the mobile first
                          approach.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | React | React
                          Responsive Carousel | Framer Motion
                        </h2>
                      </div>                      
                      <div class="project pointer-enter project-card-img">
                        <img
                        src="images/projects/project-11.png"
                        alt="Ecommerce Landing Page"
                        />
                        <div>
                        <a
                          href="https://jettdembin.github.io/ecommerce/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/ecommerce"
                          target="_blank"
                          class="pointer-enter"
                          >Github</a
                        >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-6.png"
                          alt="Hello Photo Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/photowebsite/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/photowebsite"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Hello Photo</h1>
                        <p>
                          Vanilla Javascript API built to allow users to search
                          professional photos with Pexel's API and display them to user.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">                    
                      <div class="project-card-text">
                        <h1 >Remote Now</h1>
                        <p>
                          Vanilla JavaScript job board application built to allow users to see the display
                          for real time job postings.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>                      
                      <div class="project pointer-enter project-card-img">
                        <img
                        src="images/projects/project-8.png"
                        alt="JobBoardAPI Project"
                      />
                      <div>
                        <a
                          href="https://jettdembin.github.io/jobBoardApi/"
                          target="_blank"
                          class="pointer-enter"
                          >Demo</a
                        >
                        <a
                          href="https://github.com/jettdembin/jobBoardApi"
                          class="pointer-enter"
                          target="_blank"
                          >Github</a
                        >
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-7.png"
                          alt="Beyond Beats"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/beatMaker/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/beatMaker"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Beyond Beats</h1>
                        <p>
                          Vanilla JavaScript Beat Maker application programmed with OOP concepts. Allows users to create beats with a kicker, high-hat, and snare. User may increase, decrease, and change the tempo of the beat.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-1.png"
                          alt="MockYoutube Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/mockyoutubeapi/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/mockyoutubeapi"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >MockYoutube</h1>
                        <p>
                          Vanilla Javascript application built to allow users to search
                          current videos with Youtube's API and display them on a
                          page.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>
                  <div class="project-card">
                    <div class="project-card-right">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-2.png"
                          alt="Quiz Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/quizApp/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/quizApp"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >Ecommerce Quiz</h1>
                        <p>
                          Vanilla Javascript quiz application that displays
                          questions dynamically upon entering them in the
                          questions.js file. Simulates pagination with back and next button. Allows for restart of quiz and randomization of questions with each quiz.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                    <div class="project-card-left">
                      <div class="project pointer-enter project-card-img">
                        <img
                          src="images/projects/project-9.png"
                          alt="TodoList Project"
                        />
                        <div>
                          <a
                            href="https://jettdembin.github.io/todoList/"
                            target="_blank"
                            class="pointer-enter"
                            >Demo</a
                          >
                          <a
                            href="https://github.com/jettdembin/todoList"
                            class="pointer-enter"
                            target="_blank"
                            >Github</a
                          >
                        </div>
                      </div>
                      <div class="project-card-text">
                        <h1 >TodoList</h1>
                        <p>
                          Vanilla Javascript todo list. The todo list is saved to
                          the browser's local storage, and can be loaded upon page
                          refresh. The todo list can be sorted by completed,
                          uncompleted, or all. Todos also animate upon completion
                          and deletion of todos.
                        </p>
                        <h2>
                          Technologies Used: <br />HTML5 | CSS3 | JavaScript
                        </h2>
                      </div>
                    </div>
                  </div>`;
};

export const num = 2;

const projects = [
	{
		javascript: [
			{
				name: "Ecommerce Quiz",
				description:
					"Vanilla Javascript quiz application that displays questions dynamically upon entering them in the questions.js file.",
				github: "https://github.com/jettdembin/quizApp",
				demo: "https://jettdembin.github.io/quizApp/",
				img: "images/projects/project-2.png",
				tech: "HTML5 | CSS3 | JavaScript",
			},
			{
				name: "TodoList",
				description: "Vanilla Javascript todo list.",
				github: "https://github.com/jettdembin/todoList",
				demo: "https://jettdembin.github.io/todoList/",
				img: "images/projects/project-9.png",
				tech: "HTML5 | CSS3 | JavaScript",
			},
			{
				name: "Beat Maker",
				description:
					"Vanilla JavaScript Beat Maker application programmed with OOP concepts.",
				github: "https://github.com/jettdembin/beatMaker",
				demo: "https://jettdembin.github.io/beatMaker/",
				img: "images/projects/project-3.png",
				tech: "HTML5 | CSS3 | JavaScript",
			},
		],
	},

	{
		react: [
			{
				name: "Macro Meals",
				description:
					"React Application which calculates user calories basedon desired fitness goal and allows you to track your meals and see how many calories you have consumed.",
				github: "https://github.com/jettdembin/calculator",
				demo: "https://jettdembin.github.io/calculator/",
				img: "images/projects/project-4.png",
				tech: "HTML5 | CSS3 | JavaScript | React | Material UI",
			},
			{
				name: "Your Ecommerce",
				description:
					"React ecommerce landing page built with the mobile first approach.",
				github: "https://github.com/jettdembin/ecommerce",
				demo: "https://jettdembin.github.io/ecommerce/",
				img: "images/projects/project-4.png",
				tech: "HTML5 | CSS3 | JavaScript | React | React Responsive Carousel | Framer Motion",
			},
		],
	},
	{
		api: [
			{
				name: "Remote Now",
				description:
					"Vanilla JavaScript job board application built to allow users to see the display for real time job postings.",
				github: "https://github.com/jettdembin/jobBoardApi",
				demo: "https://jettdembin.github.io/jobBoardApi/",
				img: "images/projects/project-4.png",
				tech: "HTML5 | CSS3 | JavaScript",
			},
			{
				name: "MockYoutube",
				description:
					"Vanilla Javascript application built to allow users to search current videos with Youtube's API and display them on a page.",
				github: "https://github.com/jettdembin/mockyoutubeapi",
				demo: "https://jettdembin.github.io/mockyoutubeapi/",
				img: "images/projects/project-1.png",
				tech: "HTML5 | CSS3 | JavaScript | Bootstrap",
			},
			{
				name: "Hello Photo",
				description:
					"Vanilla Javascript API built to allow users to search professional photos with Pexel's API and display them to user.",
				github: "https://github.com/jettdembin/photowebsite",
				demo: "https://jettdembin.github.io/photowebsite/",
				img: "images/projects/project-5.png",
				tech: "HTML5 | CSS3 | JavaScript | Bootstrap",
			},
		],
	},
];
